package com.example.rent.sda_043_mw_intent_filter_usage;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    public static final String key = "key";

    @BindView(R.id.edit_text_input)
    protected EditText editTextInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_open)
    protected void clickOpen() {
        String value = editTextInput.getText().toString();

        // pierwszy sposob
        // do naszej aplikacji (tej drugiej) trafia tylko intenty o takim schemacie
        Uri uri = Uri.parse("dowolny:" + value);

        // drugi sposob - wtedy bez uri tylko starczy przez extrasy
//        intent.putExtra(key, value);
//        finish();

        Intent intent = new Intent(key, uri);
        // tu jeszcze sprawdzenie jeszcze potrzebne
        startActivity(intent);

    }

}
